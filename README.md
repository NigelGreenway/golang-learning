# README

This is purely the outcome of my research/training time.

To see the work, please look at the branches available via:

```bash
git branch -r | grep "origin/chapter"
```

Then, check out the chapter branch.
